import React from "react";
import { useRouter } from "next/router";
import AdminNavbar from "components/Navbars/AdminNavbar.js";
import Sidebar from "components/Sidebar/Sidebar.js";
import routes from "routes.js";

export default function Admin({ children }) {
  const router = useRouter();

  const getBrandText = () => {
    for (let i = 0; i < routes.length; i++) {
      if (router.route.indexOf(routes[i].layout + routes[i].path) !== -1) {
        return routes[i].name;
      }
    }
    return "Records";
  };
  return (
    <>
      <Sidebar />
      <div
        className="relative md:ml-64 bg-green-900"
        style={{ marginBottom: "200px", padding: "20px" }}
      >
        <AdminNavbar brandText={getBrandText()} />
        {/* Header */}
        <div
          className="relative bg-green-900 md:pt-25 pb-20 pt-4 rounded"
          style={{ backgroundColor: "#006400" }}
        >
          <div className="px-4 md:px-10 mx-auto w-full"></div>
        </div>
        <div className="px-4 md:px-10 mx-auto w-full -m-24">
          {children}
          {/* <FooterAdmin /> */}
        </div>
      </div>
    </>
  );
}
