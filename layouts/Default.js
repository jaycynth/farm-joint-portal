import React from "react";

function DefaultLayout(props) {
  React.useEffect(() => {
    document.body.classList.add("bg-default");
    // Specify how to clean up after this effect:
    return function cleanup() {
      document.body.classList.remove("bg-default");
    };
  }, []);
  return (
    <>
      {/* <Navbar transparent /> */}
      <main>
        <section className="relative w-full h-full py-40 min-h-screen">
          <div
            className="absolute top-0 w-full h-full bg-blueGray-800 bg-no-repeat bg-full"
            style={{
              backgroundImage: "url('/img/bg-2.jpg')",
            }}
          ></div>

          {props.children}
          {/* <FooterSmall absolute /> */}
        </section>
      </main>
    </>
  );
}

export default DefaultLayout;
