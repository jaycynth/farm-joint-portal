import DefaultLayout from "./Default";
import PortalLayout from "./Portal";
import React, { Component } from "react";
import Router from "next/router";
import { useCookie } from "next-universal-cookie";

const layouts = {
  default: DefaultLayout,
  portal: PortalLayout,
};

const LayoutWrapper = (props) => {
  // cookies
  const [cookies, setCookie, removeCookie] = useCookie(["accessToken"]);

  React.useEffect(async () => {
    console.log("cookies", cookies);
    if (!cookies.accessToken) {
      console.log("UNAUTHENTICATED", cookies.accessToken);
      // User is not authenticated
      Router.push("/auth/login");
    } else {
      // User is authenticated
      console.log("AUTHENTICATED", cookies.accessToken);

      if (!Router.basePath.startsWith("/admin/")) {
        Router.push("/admin/dashboard");
      }
    }
  }, []);

  const Layout = layouts[props.children.type.layout];

  if (Layout != null) {
    return <Layout {...props}>{props.children}</Layout>;
  }
  return <DefaultLayout {...props}>{props.children}</DefaultLayout>;
};

export default LayoutWrapper;
