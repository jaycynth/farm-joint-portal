import React, { useState } from "react";

// components

import CardLineChart from "components/Cards/CardLineChart.js";
import CardBarChart from "components/Cards/CardBarChart.js";
import CardPageVisits from "components/Cards/CardPageVisits.js";
import CardSocialTraffic from "components/Cards/CardSocialTraffic.js";

import { Chart } from "react-google-charts";

// layout for page

import Admin from "layouts/Admin.js";

export default function Dashboard({
  produceRateResponse,
  purchaseRateResponse,
}) {
  const [produceRate, setProduceRate] = useState(
    produceRateResponse?.produce_rate
  );
  const [purchaseRate, setPurchaseRate] = useState(
    purchaseRateResponse?.purchase_rates
  );

  const arrayData = () => {
    var arry = [];
    var dp = ["Months", "Production rate"];
    arry.push(dp);

    produceRate?.length > 0 &&
      produceRate?.map((rate) =>
        arry.push([rate.rate_id, parseInt(rate.total_cost)])
      );

    return arry;
  };

  let produceCount = 0,
    supplementCount = 0,
    feedCount = 0,
    feedCost = 0,
    supplementCost = 0,
    produceCost = 0;

  return (
    <>
      <div className="flex flex-wrap" style={{ marginTop: "200px" }}>
        <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
          <div className="rounded-t mb-0 px-4 py-3 bg-transparent">
            <div className="flex flex-wrap items-center">
              <div className="relative w-full max-w-full flex-grow flex-1">
                <h6 className="uppercase text-blueGray-400 mb-1 text-xs font-semibold">
                  Overview
                </h6>
                <h2 className="text-blueGray text-xl font-semibold">
                  Produce Rates
                </h2>
              </div>
            </div>
            <Chart
              width={"100%"}
              height={"400px"}
              chartType="LineChart"
              loader={<div>Loading Chart</div>}
              data={arrayData()}
              options={{
                hAxis: {
                  title: "Months",
                },
                vAxis: {
                  title: "Amount Earned(Ksh)",
                },
              }}
              rootProps={{ "data-testid": "1" }}
            />
          </div>
        </div>
        <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
          <div className="rounded-t mb-0 px-4 py-3 bg-transparent">
            <div className="flex flex-wrap items-center">
              <div className="relative w-full max-w-full flex-grow flex-1">
                <h6 className="uppercase text-blueGray-400 mb-1 text-xs font-semibold">
                  Performance
                </h6>
                <h2 className="text-blueGray-700 text-xl font-semibold">
                  Purchase Rates
                </h2>
              </div>
            </div>
          </div>
          {purchaseRate?.length > 0 &&
            purchaseRate.map((rates) =>
              rates.items_stat.map((types) => {
                if (types.item_type === "PRODUCE") {
                  produceCount = types.item_count;
                  produceCost = types.item_cost;
                } else if (types.item_type === "FEED") {
                  feedCount = types.item_count;
                  feedCost = types.item_cost;
                } else if (types.item_type === "SUPPLEMENT") {
                  supplementCount = types.item_count;
                  supplementCost = types.item_cost;
                }
              })
            )}
          <Chart
            className="purchase-chart"
            width={"100%"}
            height={"300px"}
            chartType="PieChart"
            loader={<div>Loading Chart</div>}
            data={[
              ["Item Type", "Percentage bought"],
              ["PRODUCE", produceCount],
              ["FEED", feedCount],
              ["SUPPLEMENT", supplementCount],
            ]}
            options={{
              title: "Purchase Records",
              is3D: true,
            }}
            rootProps={{ "data-testid": "2" }}
          />{" "}
        </div>
      </div>
      <div className="flex flex-wrap mt-4">
        <div className="relative flex flex-col min-w-0 break-words bg-white w-full mb-6 shadow-lg rounded">
          <div className="rounded-t mb-0 px-4 py-3 bg-transparent">
            <div className="flex flex-wrap items-center">
              <div className="relative w-full max-w-full flex-grow flex-1">
                <h6 className="uppercase text-blueGray-400 mb-1 text-xs font-semibold">
                  Total Costs Accumulation
                </h6>
                <h2 className="text-blueGray-700 text-xl font-semibold">
                  Purchase Costs
                </h2>
              </div>
            </div>
            <table className="min-w-full divide-y divide-gray-200">
              <thead className="bg-gray-50">
                <tr>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Feed
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Produce
                  </th>
                  <th
                    scope="col"
                    className="px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider"
                  >
                    Supplement
                  </th>
                </tr>
              </thead>
              <tbody className="bg-white divide-y divide-gray-200">
                <tr>
                  <td className="px-6 py-4 whitespace-nowrap">{feedCost}</td>
                  <td className="px-6 py-4 whitespace-nowrap">{produceCost}</td>
                  <td className="px-6 py-4 whitespace-nowrap">
                    {supplementCost}
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
}

const listProduceRate = async () => {
  var year = new Date().getFullYear();
  var month = new Date().getMonth();

  var arr = [];

  var timestamp1 = new Date().getTime() / 1000;
  console.log("arr", Math.trunc(timestamp1));

  arr.push(Math.trunc(timestamp1));

  for (let i = 0; i < 3; i++) {
    var timestamp = new Date(year, month, 0).getTime() / 1000;
    arr.push(timestamp);

    if (month === 0) {
      year = year - 1;
      month = 11;
    } else {
      month = month - 1;
    }
  }

  const token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJRCI6IjI5IiwiUHJvamVjdElEIjoiIiwiTmFtZXMiOiJKdWxpZSIsIlBob25lTnVtYmVyIjoiMDcwNzEyMjM1MiIsIkVtYWlsQWRkcmVzcyI6IiIsIkdyb3VwIjoiVVNFUiIsImF1ZCI6ImFueW9uZSIsImV4cCI6MzE1MzYwMDAwMDAwMDAwMDAsImlzcyI6InVzZXIgQVBJIn0.CVFU9-6SVBFEEmjAPdYU1v_KdWExki3wnPCvyhc31_I";
  const res = await fetch(
    "http://ac400c222fe5845d5aef6540fc38273c-1839162505.us-east-2.elb.amazonaws.com/api/produce/produce_rate",
    {
      body: JSON.stringify({
        rate_filters: [
          {
            rate_id: "FEB",
            start_timestamp: arr[3],
            end_timestamp: arr[2],
          },
          {
            rate_id: "MAR",
            start_timestamp: arr[2],
            end_timestamp: arr[1],
          },
          {
            rate_id: "APR",
            start_timestamp: arr[1],
            end_timestamp: arr[0],
          },
        ],
      }),
      method: "POST",
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
    }
  );
  return await res.json();
};

const listPurchaseRate = async () => {
  const token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJRCI6IjI5IiwiUHJvamVjdElEIjoiIiwiTmFtZXMiOiJKdWxpZSIsIlBob25lTnVtYmVyIjoiMDcwNzEyMjM1MiIsIkVtYWlsQWRkcmVzcyI6IiIsIkdyb3VwIjoiVVNFUiIsImF1ZCI6ImFueW9uZSIsImV4cCI6MzE1MzYwMDAwMDAwMDAwMDAsImlzcyI6InVzZXIgQVBJIn0.CVFU9-6SVBFEEmjAPdYU1v_KdWExki3wnPCvyhc31_I";
  const res = await fetch(
    "http://ac400c222fe5845d5aef6540fc38273c-1839162505.us-east-2.elb.amazonaws.com/api/purchases/purchase_rate",
    {
      body: JSON.stringify({
        item_types: ["FEED", "PRODUCE", "SUPPLEMENT"],
      }),
      method: "POST",
      headers: {
        Authorization: "Bearer " + token,
        "Content-Type": "application/json",
      },
    }
  );
  return await res.json();
};

export const getStaticProps = async () => {
  const produceRateResponse = await listProduceRate();
  const purchaseRateResponse = await listPurchaseRate();

  return {
    props: {
      produceRateResponse,
      purchaseRateResponse,
    },
  };
};

Dashboard.layout = "portal";
