import React, { useState } from "react";
import Admin from "layouts/Admin.js";

import MUIDataTable from "mui-datatables";
import Link from "next/link";

export default function Farmanimal({ response }) {
  console.log(response);
  const [farmanimals, setFarmanimal] = useState(response?.farmanimal_list);

  const [responsive, setResponsive] = useState("vertical");
  const [tableBodyHeight, setTableBodyHeight] = useState("400px");
  const [tableBodyMaxHeight, setTableBodyMaxHeight] = useState("");

  const options = {
    filter: true,
    filterType: "dropdown",
    responsive,
    // tableBodyHeight,
    tableBodyMaxHeight,
    setFilterChipProps: (colIndex, colName, data) => {
      return {
        color: "primary",
        variant: "outlined",
        className: "testClass123",
      };
    },
  };

  const columns = [
    {
      name: "birth_date",
      label: "Birth Date",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "tag",
      label: "Tag",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "status",
      label: "Status",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "calfingCount",
      label: "Calfing Count",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "each_price",
      label: "Each Price",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "action",
      label: "Action",
      options: {
        filter: false,
        sort: false,
        empty: true,
        customBodyRenderLite: (dataIndex) => {
          return (
            <div>
              <Link
                href={`/admin/healthrecords/${farmanimals[dataIndex].farmanimal_id}`}
              >
                <h4
                  style={{
                    color: "#006400",
                  }}
                >
                  View Health Records
                </h4>
              </Link>
            </div>
          );
        },
      },
    },
  ];

  return (
    <>
      <div className="md:container md:mx-auto" style={{ marginTop: "150px" }}>
        <div className="rounded rounded-t-lg overflow-hidden shadow max-w-xs my-3">
          <MUIDataTable
            title={"Farm Animal Records"}
            data={farmanimals}
            columns={columns}
            options={options}
          />
        </div>
      </div>
    </>
  );
}

const listFarmanimal = async () => {
  const token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJRCI6IjI5IiwiUHJvamVjdElEIjoiIiwiTmFtZXMiOiJKdWxpZSIsIlBob25lTnVtYmVyIjoiMDcwNzEyMjM1MiIsIkVtYWlsQWRkcmVzcyI6IiIsIkdyb3VwIjoiVVNFUiIsImF1ZCI6ImFueW9uZSIsImV4cCI6MzE1MzYwMDAwMDAwMDAwMDAsImlzcyI6InVzZXIgQVBJIn0.CVFU9-6SVBFEEmjAPdYU1v_KdWExki3wnPCvyhc31_I";
  const res = await fetch(
    "http://ac400c222fe5845d5aef6540fc38273c-1839162505.us-east-2.elb.amazonaws.com/api/farmanimal",
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
  return await res.json();
};

export const getStaticProps = async () => {
  const response = await listFarmanimal();
  return {
    props: {
      response,
    },
  };
};

Farmanimal.layout = "portal";
