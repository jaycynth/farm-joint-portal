import React, { useState } from "react";
import Admin from "layouts/Admin.js";

import MUIDataTable from "mui-datatables";

export default function Feed({ response }) {
  const [feed, setFeed] = useState(response?.feed_list);

  const [responsive, setResponsive] = useState("vertical");
  const [tableBodyHeight, setTableBodyHeight] = useState("400px");
  const [tableBodyMaxHeight, setTableBodyMaxHeight] = useState("");

  const options = {
    filter: true,
    filterType: "dropdown",
    responsive,
    // tableBodyHeight,
    tableBodyMaxHeight,
    setFilterChipProps: (colIndex, colName, data) => {
      return {
        color: "primary",
        variant: "outlined",
        className: "testClass123",
      };
    },
  };

  const columns = [
    {
      name: "date",
      label: "Date",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "name",
      label: "Name",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "quantity",
      label: "Quantity",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "price",
      label: "Price",
      options: {
        filter: true,
        sort: true,
      },
    },
  ];

  return (
    <>
      <div class="md:container md:mx-auto" style={{ marginTop: "150px" }}>
        <div class="rounded rounded-t-lg overflow-hidden shadow max-w-xs my-3">
          {" "}
          <MUIDataTable
            title={"Feed Records"}
            data={feed}
            columns={columns}
            options={options}
          />
        </div>
      </div>
    </>
  );
}

const listFeed = async () => {
  const token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJRCI6IjI5IiwiUHJvamVjdElEIjoiIiwiTmFtZXMiOiJKdWxpZSIsIlBob25lTnVtYmVyIjoiMDcwNzEyMjM1MiIsIkVtYWlsQWRkcmVzcyI6IiIsIkdyb3VwIjoiVVNFUiIsImF1ZCI6ImFueW9uZSIsImV4cCI6MzE1MzYwMDAwMDAwMDAwMDAsImlzcyI6InVzZXIgQVBJIn0.CVFU9-6SVBFEEmjAPdYU1v_KdWExki3wnPCvyhc31_I";

  const res = await fetch(
    "http://ac400c222fe5845d5aef6540fc38273c-1839162505.us-east-2.elb.amazonaws.com/api/feed",
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
  return await res.json();
};

export const getStaticProps = async () => {
  const response = await listFeed();
  return {
    props: {
      response,
    },
  };
};

Feed.layout = "portal";
