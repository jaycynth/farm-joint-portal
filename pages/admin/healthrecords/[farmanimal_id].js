import React, { useEffect, useState } from "react";
import Router, { useRouter } from "next/router";
import Admin from "layouts/Admin.js";
import MUIDataTable from "mui-datatables";

export default function HealthRecord() {
  const { query } = useRouter();

  const [healthrecords, setHealthrecords] = useState();

  useEffect(async () => {
    const response = await listRecords(query.farmanimal_id);
    if (!response.code) {
      setHealthrecords(response.health_list);
    }
  }, [query.farmanimal_id]);

  const [responsive, setResponsive] = useState("vertical");
  const [tableBodyHeight, setTableBodyHeight] = useState("400px");
  const [tableBodyMaxHeight, setTableBodyMaxHeight] = useState("");

  const options = {
    filter: true,
    filterType: "dropdown",
    responsive,
    // tableBodyHeight,
    tableBodyMaxHeight,
    setFilterChipProps: (colIndex, colName, data) => {
      return {
        color: "primary",
        variant: "outlined",
        className: "testClass123",
      };
    },
  };
  const columns = [
    {
      name: "date",
      label: "Date",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "procedire_type",
      label: "Procedure Type",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "description",
      label: "Description",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "vet_name",
      label: "vet Name",
      options: {
        filter: true,
        sort: true,
      },
    },
  ];

  return (
    <>
      <div className="md:container md:mx-auto" style={{ marginTop: "150px" }}>
        <button
          style={{ margin: "20px", backgroundColor: "#1E90FF" }}
          className="bg-blue hover:bg-blue-dark text-white font-bold py-2 px-2 rounded"
          onClick={(e) => {
            Router.push("/admin/farmanimal");
          }}
        >
          Go Back
        </button>
        <div className="rounded rounded-t-lg overflow-hidden shadow max-w-xs my-3">
          <MUIDataTable
            title={"Health Records"}
            data={healthrecords}
            columns={columns}
            options={options}
          />
        </div>
      </div>
    </>
  );
}

const listRecords = async (farmanimalId) => {
  const token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJRCI6IjI5IiwiUHJvamVjdElEIjoiIiwiTmFtZXMiOiJKdWxpZSIsIlBob25lTnVtYmVyIjoiMDcwNzEyMjM1MiIsIkVtYWlsQWRkcmVzcyI6IiIsIkdyb3VwIjoiVVNFUiIsImF1ZCI6ImFueW9uZSIsImV4cCI6MzE1MzYwMDAwMDAwMDAwMDAsImlzcyI6InVzZXIgQVBJIn0.CVFU9-6SVBFEEmjAPdYU1v_KdWExki3wnPCvyhc31_I";
  const res = await fetch(
    `http://ac400c222fe5845d5aef6540fc38273c-1839162505.us-east-2.elb.amazonaws.com/api/health?farmanimal_id="+ ${farmanimalId}`,
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
  return await res.json();
};

HealthRecord.layout = "portal";
