import React, { useState } from "react";
import MUIDataTable from "mui-datatables";

import Admin from "layouts/Admin.js";

export default function Produce({ response }) {
  console.log(response);
  const [produce, setProduce] = useState(response?.produce_list);

  const [responsive, setResponsive] = useState("vertical");
  const [tableBodyHeight, setTableBodyHeight] = useState("400px");
  const [tableBodyMaxHeight, setTableBodyMaxHeight] = useState("");

  const options = {
    filter: true,
    filterType: "dropdown",
    responsive,
    // tableBodyHeight,
    tableBodyMaxHeight,
    setFilterChipProps: (colIndex, colName, data) => {
      return {
        color: "primary",
        variant: "outlined",
        className: "testClass123",
      };
    },
  };

  const columns = [
    {
      name: "date",
      label: "Date",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "produce_type",
      label: "Produce Type",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "quantity",
      label: "Quantity",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "quantity_type",
      label: "Quantity Type",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "each_price",
      label: "Each Price",
      options: {
        filter: true,
        sort: true,
      },
    },
  ];

  return (
    <>
      <div class="md:container md:mx-auto" style={{ marginTop: "150px" }}>
        <div class="rounded rounded-t-lg overflow-hidden shadow max-w-xs my-3">
          <MUIDataTable
            title={"Produce Records"}
            data={produce}
            columns={columns}
            options={options}
          />
        </div>
      </div>
    </>
  );
}

const listProduce = async () => {
  const token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJRCI6IjI5IiwiUHJvamVjdElEIjoiIiwiTmFtZXMiOiJKdWxpZSIsIlBob25lTnVtYmVyIjoiMDcwNzEyMjM1MiIsIkVtYWlsQWRkcmVzcyI6IiIsIkdyb3VwIjoiVVNFUiIsImF1ZCI6ImFueW9uZSIsImV4cCI6MzE1MzYwMDAwMDAwMDAwMDAsImlzcyI6InVzZXIgQVBJIn0.CVFU9-6SVBFEEmjAPdYU1v_KdWExki3wnPCvyhc31_I";
  const res = await fetch(
    "http://ac400c222fe5845d5aef6540fc38273c-1839162505.us-east-2.elb.amazonaws.com/api/produce",
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
  return await res.json();
};

export const getStaticProps = async () => {
  const response = await listProduce();
  return {
    props: {
      response,
    },
  };
};

Produce.layout = "portal";
