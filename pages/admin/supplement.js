import React, { useState } from "react";
import Admin from "layouts/Admin.js";

import MUIDataTable from "mui-datatables";

export default function Supplement({ response }) {
  const [supplement, setSupplement] = useState(response?.supplement_list);

  const [responsive, setResponsive] = useState("vertical");
  const [tableBodyHeight, setTableBodyHeight] = useState("400px");
  const [tableBodyMaxHeight, setTableBodyMaxHeight] = useState("");

  const options = {
    filter: true,
    filterType: "textField",
    responsive,
    // tableBodyHeight,
    tableBodyMaxHeight,
    setFilterChipProps: (colIndex, colName, data) => {
      return {
        color: "primary",
        variant: "outlined",
        className: "testClass123",
      };
    },
  };
  const columns = [
    {
      name: "date_bought",
      label: "Date Bought",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "supplement_name",
      label: "Name",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "units",
      label: "Units",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "quantity",
      label: "Quantity",
      options: {
        filter: true,
        sort: true,
      },
    },
    {
      name: "price",
      label: "Price",
      options: {
        filter: true,
        sort: true,
      },
    },
  ];
  return (
    <>
      <div className="md:container md:mx-auto" style={{ marginTop: "150px" }}>
        <div className="rounded rounded-t-lg overflow-hidden shadow max-w-xs my-3">
          <MUIDataTable
            title={"Supplement Records"}
            data={supplement}
            columns={columns}
            options={options}
          />
        </div>
      </div>
    </>
  );
}
const listSupplement = async () => {
  const token =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJJRCI6IjI5IiwiUHJvamVjdElEIjoiIiwiTmFtZXMiOiJKdWxpZSIsIlBob25lTnVtYmVyIjoiMDcwNzEyMjM1MiIsIkVtYWlsQWRkcmVzcyI6IiIsIkdyb3VwIjoiVVNFUiIsImF1ZCI6ImFueW9uZSIsImV4cCI6MzE1MzYwMDAwMDAwMDAwMDAsImlzcyI6InVzZXIgQVBJIn0.CVFU9-6SVBFEEmjAPdYU1v_KdWExki3wnPCvyhc31_I";

  const res = await fetch(
    "http://ac400c222fe5845d5aef6540fc38273c-1839162505.us-east-2.elb.amazonaws.com/api/supplement",
    {
      headers: {
        Authorization: "Bearer " + token,
      },
    }
  );
  return await res.json();
};

export const getStaticProps = async () => {
  const response = await listSupplement();
  return {
    props: {
      response,
    },
  };
};

Supplement.layout = "portal";
