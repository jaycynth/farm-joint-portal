import React from "react";
import Router from "next/router";
import Auth from "layouts/Auth.js";
import { useCookie } from "next-universal-cookie";

export default function Login() {
  const [names, setNames] = React.useState("");
  const [phone, setPhone] = React.useState("");
  const [isLoading, setIsLoading] = React.useState(false);
  const [cookies, setCookie, removeCookie] = useCookie(["accessToken"]);

  const handleNamesChange = (e) => {
    // setForm({ ...form, [e.target.name]: e.target.value });

    setNames(e.target.value), console.log(e.target.value);
  };

  const handlePhoneChange = (e) => {
    // setForm({ ...form, [e.target.name]: e.target.value });

    setPhone(e.target.value), console.log(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setIsLoading(true);

    const data = await login(names, phone);

    if (!data.code) {
      //successfuly added a user
      setIsLoading(false);
      alert("Succesfully logged in");
      setCookie("accessToken", data.jwt_token, {
        path: "/",
      });
      Router.push("/admin/dashboard");
    } else {
      setIsLoading(false);
      alert(data.message);
    }
  };
  return (
    <>
      <div className="container mx-auto px-4 h-full">
        <div className="flex content-center items-center justify-center h-full">
          <div className="w-full lg:w-4/12 px-4">
            <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-200 border-0">
              <h1
                className="text-lightGreen-600 text-center font-bold"
                style={{ marginTop: "20px" }}
              >
                Farm Assist
              </h1>

              <div className="rounded-t mb-0 px-6 py-6">
                <div className="text-blueGray-400 text-center mb-3 font-bold">
                  <small>Sign In</small>
                </div>

                <hr className="mt-6 border-b-1 border-blueGray-300" />
              </div>
              <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
                <form onSubmit={handleSubmit}>
                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      Username
                    </label>
                    <input
                      type="username"
                      className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                      placeholder="Username"
                      onChange={handleNamesChange}
                    />
                  </div>

                  <div className="relative w-full mb-3">
                    <label
                      className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                      htmlFor="grid-password"
                    >
                      Phone Number
                    </label>
                    <input
                      type="phone"
                      className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                      placeholder="Phone Number"
                      onChange={handlePhoneChange}
                    />
                  </div>
                  <div>
                    <label className="inline-flex items-center cursor-pointer">
                      <input
                        id="customCheckLogin"
                        type="checkbox"
                        className="form-checkbox border-0 rounded text-blueGray-700 ml-1 w-5 h-5 ease-linear transition-all duration-150"
                      />
                      <span className="ml-2 text-sm font-semibold text-blueGray-600">
                        Remember me
                      </span>
                    </label>
                  </div>

                  <div className="text-center mt-6">
                    <button
                      className="bg-green-800 text-white active:bg-green-600 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150"
                      type="submit"
                      disabled={isLoading}
                    >
                      Sign In
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}
const login = async (username, phone) => {
  const res = await fetch(
    "http://ac400c222fe5845d5aef6540fc38273c-1839162505.us-east-2.elb.amazonaws.com/api/users/action/login",
    {
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        user: {
          user_name: username,
          phone_number: phone,
        },
      }),
      method: "POST",
    }
  );

  return await res.json();
};

Login.layout = Auth;
