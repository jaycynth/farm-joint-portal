import React from "react";
import Admin from "layouts/Admin.js";

export default function Register() {
  const [isLoading, setLoading] = React.useState(false);

  const [names, setNames] = React.useState("");
  const [phone, setPhone] = React.useState("");

  const handleNamesChange = (e) => {
    // setForm({ ...form, [e.target.name]: e.target.value });

    setNames(e.target.value), console.log(e.target.value);
  };

  const handlePhoneChange = (e) => {
    // setForm({ ...form, [e.target.name]: e.target.value });

    setPhone(e.target.value), console.log(e.target.value);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    setLoading(true);

    const data = await register(names, phone);

    if (!data.code) {
      //successfuly added a user
      alert("Succesfully added user");
    } else {
      alert("error creating user");
    }
  };

  return (
    <>
      <div
        className="container mx-auto px-4 h-full"
        style={{ marginTop: "150px" }}
      >
        <div className="flex content-center items-center justify-center h-full">
          <div className="relative flex flex-col min-w-0 break-words w-full mb-6 shadow-lg rounded-lg bg-blueGray-200 border-0">
            <div className="rounded-t mb-0 px-6 py-6">
              <div className="text-blueGray-400 text-center mb-3 font-bold">
                <small>Create New User</small>
              </div>
              <hr className="mt-6 border-b-1 border-blueGray-300" />
            </div>
            <div className="flex-auto px-4 lg:px-10 py-10 pt-0">
              <form onSubmit={handleSubmit}>
                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    Name
                  </label>
                  <input
                    type="email"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    placeholder="Name"
                    onChange={handleNamesChange}
                  />
                </div>

                {/* <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    Email
                  </label>
                  <input
                    type="email"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    placeholder="Email"
                  />
                </div> */}

                <div className="relative w-full mb-3">
                  <label
                    className="block uppercase text-blueGray-600 text-xs font-bold mb-2"
                    htmlFor="grid-password"
                  >
                    Phone Number
                  </label>
                  <input
                    type="phonenumber"
                    className="border-0 px-3 py-3 placeholder-blueGray-300 text-blueGray-600 bg-white rounded text-sm shadow focus:outline-none focus:ring w-full ease-linear transition-all duration-150"
                    placeholder="Phone Number"
                    onChange={handlePhoneChange}
                  />
                </div>

                <div className="text-center mt-6">
                  <button
                    className="bg-green-800 text-white active:bg-green-600 text-sm font-bold uppercase px-6 py-3 rounded shadow hover:shadow-lg outline-none focus:outline-none mr-1 mb-1 w-full ease-linear transition-all duration-150"
                    type="submit"
                  >
                    Create Account
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

const register = async (username, phone) => {
  const res = await fetch(
    "http://ac400c222fe5845d5aef6540fc38273c-1839162505.us-east-2.elb.amazonaws.com/api/users",
    {
      body: JSON.stringify({}),
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        user: {
          user_name: username,
          phone_number: phone,
        },
      }),
      method: "POST",
    }
  );

  return await res.json();
};

Register.layout = "portal";
