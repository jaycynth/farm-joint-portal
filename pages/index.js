/* eslint-disable react/jsx-no-target-blank */
import React, { useEffect } from "react";
import Link from "next/link";
import Router from "next/router";
import { useCookie } from "next-universal-cookie";

export default function Index() {
  const [cookies, setCookie, removeCookie] = useCookie(["accessToken"]);

  useEffect(() => {
    if (!cookies.accessToken) {
      console.log("UNAUTHENTICATEDI", cookies.accessToken);
      // User is not authenticated
      Router.push("/auth/login");
    } else {
      // User is authenticated
      console.log("AUTHENTICATEDI", cookies.accessToken);

      Router.push("/admin/dashboard");
    }
  });

  return <div />;
}
