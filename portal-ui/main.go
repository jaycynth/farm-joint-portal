package main

import (
	"flag"
	"log"
	"net/http"
)

var (
	port = flag.String("port", ":80", "Port to listen format is :port")
)

func main() {
	flag.Parse()

	handler := http.FileServer(http.Dir("./out/"))

	log.Printf("Static file server started on port %s\n", *port)

	log.Fatalln(http.ListenAndServe(*port, handler))
}
