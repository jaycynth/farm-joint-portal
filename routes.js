var routes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "ni ni-chart-bar-32 text-primary",
    layout: "/admin",
    roles: ["SUPER_ADMIN"],
    needsAuth: true,
  },
  {
    path: "/farmanimal",
    name: "Farm Animals",
    icon: "ni ni-curved-next text-blue",
    layout: "/admin",
    roles: ["SUPER_ADMIN"],
    needsAuth: true,
  },

  {
    path: "/feed",
    name: "Feeds",
    icon: "ni ni-single-02 text-orange",
    layout: "/admin",
    roles: ["SUPER_ADMIN"],
    needsAuth: true,
  },
  {
    path: "/produce",
    name: "Produce",
    icon: "ni ni-tv-2 text-orange",
    layout: "/admin",
    roles: ["SUPER_ADMIN"],
    needsAuth: true,
  },
  {
    path: "/supplement",
    name: "Supplements",
    icon: "ni ni-bullet-list-67 text-red",
    layout: "/admin",
    roles: ["SUPER_ADMIN"],
    needsAuth: true,
  },
];
export default routes;
